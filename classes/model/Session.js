const { ORM } = require('kohanajs');

class Session extends ORM {
  sid = null;

  expired = 0;

  sess = null;

  static joinTablePrefix = 'session';

  static tableName = 'sessions';

  static fields = new Map([
    ['sid', 'String!'],
    ['expired', 'Int!'],
    ['sess', 'String'],
  ]);
}

module.exports = Session;
